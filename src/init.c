#include "define.h"
#include "lcd.h"
#include "init.h"
#include <avr/io.h>			   
#include <avr/interrupt.h>		
#include <util/delay.h>			
#include <avr/pgmspace.h>


void init(void)
{
	
//Инициализация мк и периферии.
/*
;=======================================================================
; ddr(x) - задает чем будет вывод:
; 1 - выход
; 0 - вход
;=======================================================================
; port(x) - включает подтяжку входа:
; 1 - подтяжка включена
; 0 - подтяжка выключена
; значение 0 так же устанавливает выход в 0 при включении мик-ра, если 
; вывод сконфигурирован выходом, а 1 включает выход относительно минуса питания
;=======================================================================*/
	                 /*НАСТРОЙКА ПОРТОВ*/
#if 1

DDRA  = 0b11111111;			//если 0 значит вход
PORTA = 0b00000000;			//если 1 подтяжка включена

DDRB  = 0b11100000;       
PORTB = 0b00011111;       

DDRC  = 0b11111111;
PORTC = 0b00000000;

DDRD  = 0b10110000;       
PORTD = 0b01001111;       

#endif
//=========================================================================================
                      /*Настройка таймера TCNT0*/
#if 0
/* 
SFIOR - сброс предделителя
TCNT0 - счетный регистр
TCCR0 - регистр настройки таймера
TIFR  - регистр флагов прерывание
TIMSK - регистр включения прерываний
OCR0  - регистр сравнения с регистром счета TCNT0
000-таймер выкл, 001-0, 010-8,011 - 64, 100 - 256, 101 - 1024 */
//000-таймер выкл, 001-0, 010-8,011 - 64, 100 - 256, 101 - 1024
TCCR0=(0<<FOC0)|(0<<WGM00)|(0<<COM01)|(0<<COM00)|(1<<WGM01)|(0<<CS02)|(1<<CS01)|(1<<CS00);

TIMSK|=(1<<OCIE0)|(0<<TOIE0); 
OCR0=0xF9;
/*
TIMSK - установка разрешений прерываний 
		OCIE0 - по совпадению регистра совпадения с регистром счета OCR0
		TOIE0  - по переполнению счетного регистра TCNT0 */
#endif
//=========================================================================================
					  /*Настройка таймера TCNT1*/
#if 0
//000-таймер выкл, 001-0, 010-8,011 - 64, 100 - 256, 101 - 1024
TCCR1A=(0<<COM1A1)|(0<<COM1A0)|(0<<COM1B1)|(0<<COM1B0)|(0<<FOC1A)|(0<<FOC1B)|(0<<WGM11)|(0<<WGM10);

TCCR1B|=(1<<ICNC1)|(0<<ICES1)|(0<<WGM13)|(0<<WGM12)|(0<<CS12)|(1<<CS11)|(1<<CS10);
	  /*ICES1 = 0 активный спадающий фронт
		ICES1 = 1 активный нарастающий фронт
		считаем с частотой в 8 тиков = 0.5 мксек
		*/
	 
TIMSK|=(1<<TICIE1)|(0<<OCIE1A)|(0<<OCIE1B)|(0<<TOIE1); 
	  /*TICIE1 - разрешение прерывания по событию "захват таймера"
		OCIE1A - разрешение прерывания по совпадению с регистром сравнения А
		OCIE1B - разрешение прерывания по совпадению с регистром сравнения В
		TOIE1  - разрешение прерывания по переполнению счетного регистра*/
#endif
//=========================================================================================
					  /*Настройка таймера TCNT2*/ 
#if 1// 
//000-таймер выкл, 001-0, 010-8,011 - 32, 100 - 64, 101 - 128, 110 - 256, 111 - 1024.
TCCR2=(0<<FOC2)|(1<<WGM20)|(1<<COM21)|(0<<COM20)|(1<<WGM21)|(1<<CS22)|(0<<CS21)|(0<<CS20); 
ASSR=(0<<AS2)|(0<<TCN2UB)|(0<<OCR2UB)|(0<<TCR2UB); 
TIMSK|=(0<<OCIE2)|(0<<TOIE2);
OCR2=255;
#endif
//=========================================================================================
					  /*АНАЛОГОВЫЙ КОМПАРАТОР*/
#if 1
ACSR=(1<<ACD)|(0<<ACBG)|(0<<ACO)|(0<<ACI)|(0<<ACIE)|(0<<ACIC)|(0<<ACIS1)|(0<<ACIS0);

/*
ACD - включение компаратора 0-включен, 1-выключен
ACBG - Подключение к неинв. входу (AIN0) внутренний ИОН 0-не подключен 1-подключен
ACO - если AIN0>AIN1, тогда ACO = 1.
ACI - флаг прерывания
ACIE - разрешение прерывания
ACIC - подкл. к блоку захвата Т1

Условия генрации прерывания         
ACIS1  ACIS0 
 0       0      любое изменение ACO
 0       1      резерв
 1       0      с 1 на 0
 1       1      с 0 на 1
 Если U на AIN1 < чем на AIN0 тогда выход компаратора ACO будет равен 1 
 и наоборот.*/ 
#endif
//=========================================================================================
					  /*Настройка сторожевого таймера*/
#if 0

WDTCR =(0<<WDTOE)|(1<<WDE)|(1<<WDP2)|(1<<WDP1)|(1<<WDP0); 

#endif
//=========================================================================================
		
_delay_ms(150);						
LCDinit();		 //Инициализация          LCD дисплея 314байт

sei();			 //Разрешаем прерывания.
		 								
}

