/*
 * Mega_LCD.c
 *
 *  Created: 12.01.2014 16:27:33
 *  Библиотека с поддержкой русских символов.
 */ 

#include "define.h"
#include "init.h"
#include "lcd.h"
#include <avr/pgmspace.h>
#include <util/delay.h>	

uint8_t *pBuf;//определяем переменную указатель на буфер
uint8_t count; 	
	 				
int main(void)
{
	init();//Инициализация МК и остальной обвязки.
	pBuf=BCD_GetPointerBuf();//инициализация переменной pBuf 
							 //для вывода данных на LCD.
    while(1)
	{
		BCD_3(count);
		LCDstring_of_sramXY(pBuf,5,0);
		count++;
		_delay_ms(200);
	}
}




